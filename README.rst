***********************
Python Project Template
***********************

This is a template for Python projects based on the wonderful template here: `https://github.com/audreyr/cookiecutter-pypackage/ <https://github.com/audreyr/cookiecutter-pypackage/>`_.

Usage
=====

``cookiecutter https://gitlab.com/paulni/python-template.git`` 
